'''
The idea is to fail as much as possible, if you pass, then the protocol must be
right
'''


from pymodbus.client.sync import ModbusTcpClient
from random import choice, randint
import time

shared = {
    'IP': '127.0.0.1',
    'Port': '9999',
    'start_coils': 200,
    'start_registers': 20,
    'num_reg': 10,
    'server': 1,
    'client': '',
}


def prom_to_err(result):
    '''Rust: Fn<T>(T) -> unwrap(Result<T, err>)
    where: T isError'''
    if (result.isError()):
        print(result)
        assert(False)
    else:
        return result


def get_coils(length: int) -> list:
    result = shared['client'].read_coils(
        shared['start_coils'],
        length,
        unit=shared['server']
    )
    return prom_to_err(result).bits


def get_registers(length: int) -> list:
    result = shared['client'].read_holding_registers(
        shared['start_registers'],
        length,
        unit=shared['server']
    )
    return prom_to_err(result).registers


def comp_lists(l1: list, l2: list) -> bool:
    return False not in [l1_item == l2_item
                         for l1_item, l2_item in zip(l1, l2)]


def check_write_coils():
    for each in range(shared['num_reg'] + 1):
        vec = [choice([True, False]) for _ in range(each)]
        prom_to_err(
            shared['client'].write_coils(
                shared['start_coils'],
                vec,
                unit=shared['server']
            )
        )
        rec = get_coils(each)
        assert(comp_lists(rec, vec))


def check_write_registers():
    for each in range(shared['num_reg'] + 1):
        vec = [randint(0, 65535) for _ in range(each)]
        prom_to_err(
            shared['client'].write_registers(
                shared['start_registers'],
                vec,
                unit=shared['server']
            )
        )
        rec = get_registers(each)
        assert(comp_lists(rec, vec))


def check_mask_register():
    items = [2**i for i in range(16)]

    # Set all to ones
    shared['client'].write_registers(
        shared['start_registers'],
        [0],
        unit=shared['server']
    )
    for item in items:
        prom_to_err(
            shared['client'].mask_write_register(
                shared['start_registers'],
                0xFFFF - item,
                item,
                unit=shared['server']
            )
        )
        res = get_registers(1)[0]
        assert(res == item * 2 - 1)

    # Set all to zeros
    for item in items:
        prom_to_err(
            shared['client'].mask_write_register(
                shared['start_registers'],
                0xFFFF - item,
                0,
                unit=shared['server']
            )
        )
        res = get_registers(1)[0]
        assert(res == 0xFFFF - (item * 2 - 1))

    # Check note
    for item in items:
        prom_to_err(
            shared['client'].mask_write_register(
                shared['start_registers'],
                0,
                item,
                unit=shared['server']
            )
        )
        res = get_registers(1)[0]
        assert(res == item)


def checkable_errors():
    result = shared['client'].mask_write_register(
        shared['start_registers'] + shared['num_reg'],
        0,
        0,
        unit=shared['server']
    )
    assert(result.isError())
    time.sleep(1)
    result = shared['client'].mask_write_register(
        shared['start_registers'] - 1,
        0,
        0,
        unit=shared['server']

    )
    assert(result.isError())
    time.sleep(1)
    result = shared['client'].write_registers(
        shared['start_registers'] + shared['num_reg'] - 2,
        [0, 0, 0],
        unit=shared['server']
    )
    assert(result.isError())
    time.sleep(1)
    result = shared['client'].write_registers(
        shared['start_registers'] - 2,
        [0, 0, 0],
        unit=shared['server']
    )
    assert(result.isError())
    time.sleep(1)
    result = shared['client'].write_coils(
        shared['start_registers'] + shared['num_reg'] - 2,
        [True, False, True],
        unit=shared['server']
    )
    assert(result.isError())
    time.sleep(1)
    result = shared['client'].write_coils(
        shared['start_registers'] - 2,
        [True, False, True],
        unit=shared['server']
    )
    assert(result.isError())


def main():
    shared['client'] = ModbusTcpClient(shared['IP'], shared['Port'])
    shared['client'].connect()
    check_write_coils()
    check_write_registers()
    check_mask_register()
    checkable_errors()
    shared['client'].close()
    print("All tests passed")


if __name__ == '__main__':
    main()
